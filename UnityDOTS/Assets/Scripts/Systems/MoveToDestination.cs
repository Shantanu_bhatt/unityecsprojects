using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class MoveToDestination : SystemBase
{
    protected override void OnUpdate()
    {
        // Assign values to local variables captured in your job here, so that it has
        // everything it needs to do its work when it runs later.
        // For example,
        float deltaTime = Time.DeltaTime;

        // This declares a new kind of job, which is a unit of work to do.
        // The job is declared as an Entities.ForEach with the target components as parameters,
        // meaning it will process all entities in the world that have both
        // Translation and Rotation components. Change it to process the component
        // types you want.
        
        
        
        Entities.ForEach((ref Translation translation,ref Rotation rotation,in MovementSpeed movementSpeed,in Destination destination) => {
            if(math.all(destination.Value==translation.Value)){return;}
            float3 toDestination=destination.Value-translation.Value;
            rotation.Value=quaternion.LookRotation(toDestination,new float3(0,1,0));
            float3 movement =math.normalize(toDestination)*movementSpeed.Value*deltaTime;
            if(math.length(movement)>=math.length(toDestination))
            {
                translation.Value=destination.Value;
            }
            else
            translation.Value+=movement;
        }).ScheduleParallel();
    }
}
